from flask_script import Manager
from xenlib import app
from xenlib import config
from xenlib.management.commands.user import Users
from xenlib.management.commands.project import Projects
from xenlib.management.commands.module import Modules
from xenlib.management.commands.info import Infos

def execute():
    manager = Manager(app, 
                  description='Xen Developer Tools by Xentinel', 
                  usage='xen <category> <action> [-<option>=<option value>]', 
                  with_default_commands=False)
    manager.help_args = ('-?', '--help')
    #manager.add_command('organization', Projects.project)
    manager.add_command('project', Projects.project)
    manager.add_command('info', Infos.info)
    #manager.add_command('task', Modules.module)
    #manager.add_command('api', Users.user)
    manager.run()