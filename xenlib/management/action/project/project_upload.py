import json
import os
import pysftp
from flask_script import Manager, Command, Option
from xenlib.libs.xendir import Dir

project_name = 'ubt'
local_project_base_dir = os.path.join('E:\\xentools-project\\xen-repo\\project', project_name)
remote_dev_host = 'z600' 
remote_dev_username = 'dev1'
remote_dev_password = '123'
dirf = Dir(local_project_base_dir) #, only_file='.only')
#print(dirf.json_dumps())

class Upload(Command):
    help_args = ('-h', '--help')
    help = "Upload Project"
    option_list = (
        Option("-n","--name", required=False, dest='name', help='project name'),
        Option("-r","--remote", required=False, dest='remote_git', help='project\'s remote git')
    )

    def mkdir_p(self, sftp, remote_directory):
        """Change to this directory, recursively making new folders if needed.
        Returns True if any folders were created."""
        if remote_directory == '/':
            # absolute path so change directory to root
            sftp.chdir('/')
            return
        if remote_directory == '':
            # top-level relative directory must exist
            return
        try:
            sftp.chdir(remote_directory) # sub-directory exists
        except IOError:
            dirname, basename = os.path.split(remote_directory.rstrip('/'))
            self.mkdir_p(sftp, dirname) # make parent directories
            sftp.mkdir(basename) # sub-directory missing, so created it
            sftp.chdir(basename)
            return True

    def is_sftp_dir_exists(self, sftp, path):
        try:
            sftp.stat(path)
            return True
        except Exception:
            return False

    #def copy_to_remote(self, name):
    def run(self, name=None, remote_git=None):
        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None   
        sftp = pysftp.Connection(host=remote_dev_host, username=remote_dev_username, password=remote_dev_password, cnopts=cnopts)
        remote_project_path = '/' + project_name
        print(remote_project_path)
        sftp.chdir('.')

        for f in dirf.list_files:
            abs_f = os.path.join(dirf.directory, f)
            #if '\\.' not in abs_f and '\\_' not in abs_f:
                # if diatas bersifat temporary. karena tidak support file dengan awalan . atau __
            target_rel_file = abs_f.replace(local_project_base_dir,'').replace('\\','/')
            target_rel_path = os.path.dirname(target_rel_file)
            print('Uploading file {} to DEV {} ...'.format(os.path.basename(abs_f), target_rel_path))
            if not self.is_sftp_dir_exists(sftp, target_rel_path):
                self.mkdir_p(sftp, target_rel_path)
                sftp.chdir(target_rel_path)
            sftp.chdir('.')
            sftp.put(abs_f, target_rel_file)


