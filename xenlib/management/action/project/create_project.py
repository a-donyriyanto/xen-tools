import time
import sys
import os
import git
from dotenv import load_dotenv
from flask_script import Manager, Command, Option

BASE_DIR = os.path.dirname(os.path.realpath(__name__))
BASE_DIR_ENV = os.path.join(BASE_DIR, '.env')
XENPROJECT_ROOT_ENV = os.path.join(BASE_DIR, '..', '.env')

load_dotenv(BASE_DIR_ENV)
load_dotenv(XENPROJECT_ROOT_ENV)

LOCAL_REPO = os.getenv('LOCAL_REPO', os.path.join(BASE_DIR, '..', 'repo-local'))
REMOTE_REPO = os.getenv('REMOTE_REPO', os.path.join(BASE_DIR, '..', 'repo-remote'))

class Create(Command):
    help_args = ('-h', '--help')
    help = "Create Project"
    option_list = (
        Option("-n","--name", required=True, dest='name', help='project name'),
        Option("-r","--remote", required=False, dest='remote_git', help='project\'s remote git')
    )

    def create_file_init(self,dir=None):
        try:
            with open(os.path.join(dir, 'README.txt'), "a") as f:
                f.write("This is README")
                f.close()
        except Exception as e:
            raise "error creating project"

    def run(self,name=None, remote_git=None):
        """Create new app project folder"""
        print("Creating project folder")
        if not os.path.exists(LOCAL_REPO):
            os.mkdir(LOCAL_REPO)
        dir = os.path.join(LOCAL_REPO, name)
        os.mkdir(dir)
        self.create_file_init(dir)
        if remote_git is not None:
            repo = git.Repo.init(dir)
            origin = repo.create_remote('origin', remote_git)
            origin.fetch()
            print("... Git init finished ...")
            print("... Git remote finished ...")
        else:
            git.Repo.init(dir)
            print("... Git init finished ...")
        print("... Successfully created project {} ...".format(name))
