import time
import sys
import os

from flask_script import Manager, Command, Option
from pycrosskit.envariables import SysEnv

BASE_DIR = os.path.dirname(os.path.realpath(__name__))
BASE = os.path.join(BASE_DIR, 'xen-project')

class Select(Command):
    help_args = ('-h', '--help')
    help = "Select Project"
    option_list = (
        Option("-n","--name", required=True, dest='name', help='project name'),
        Option("-r","--remote", required=False, dest='remote_git', help='project\'s remote git')
    )

    def run(self, name=None, remote_git=None):
        """Create your app project folder"""
        print("Project {} selected".format(name))
        #os.environ["XEN_PROJECT_SELECTED"] = name
        SysEnv.set_var('XEN_PROJECT_SELECTED',name)
        print(SysEnv.get_var('XEN_PROJECT_SELECTED'))