import os
from flask_script import Manager, Command
from dotenv import load_dotenv

from xenlib.management.action.project.create_project import BASE_DIR

class Infos(Command):
    info = Manager(usage="show info")

    @info.command
    def show():
        """ Show Info """
        BASE_DIR = os.path.dirname(os.path.realpath(__name__))
        BASE_DIR_ENV = os.path.join(BASE_DIR, '.env')
        XENPROJECT_ROOT_ENV = os.path.join(BASE_DIR, '..', '.env')

        load_dotenv(BASE_DIR_ENV)
        load_dotenv(XENPROJECT_ROOT_ENV)

        LOCAL_REPO = os.getenv('LOCAL_REPO', os.path.join(BASE_DIR, '..', 'repo-local'))
        REMOTE_REPO = os.getenv('REMOTE_REPO', os.path.join(BASE_DIR, '..', 'repo-remote'))
        print("BASE_DIR", BASE_DIR)
        print("BASE_DIR_ENV", BASE_DIR_ENV)
        print("XENPROJECT_ROOT_ENV", XENPROJECT_ROOT_ENV)
        print("LOCAL_REPO", LOCAL_REPO)
        print("REMOTE_REPO", REMOTE_REPO)
        
        

