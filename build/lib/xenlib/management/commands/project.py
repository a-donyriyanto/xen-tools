import sys
import time
import shutil
import os
import json
from flask_script import Manager, Command

from xenlib.libs.get_project import check_project_available
from xenlib.libs.get_project import check_project_execution

from xenlib.management.action.project.create_project import Create
from xenlib.management.action.project.install_project import Install
from xenlib.management.action.project.select_project import Select
from xenlib.management.action.project.project_upload import Upload

class Projects(Command):
    project = Manager(usage="Manage application project folder")

    project.add_command('create', Create())
    project.add_command('remove', Create())
    project.add_command('install', Install())
    project.add_command('select', Select())
    project.add_command('upload', Upload())
    
    '''
    @project.command
    def upload():
        """ Upload project to SFTP server """
        from pycrosskit.envariables import SysEnv
        try:
            selected_project = SysEnv.get_var('XEN_PROJECT_SELECTED')
            print('-->', selected_project)
            if selected_project != '':
                projectu.copy_to_remote()
            else:
                print('Please select project first1')
        except Exception:
            print('Please select project first2')
    '''

    @project.command
    def available():
        """ Check Module Available """

        list_project = check_project_available()
        print("Project Available : ")
        for available in list_project:
            print("  "+available['projects_name']+" -> "+available['projects_git_remoteurl']+" ("+available['projects_git_defaultbranch']+")")

    @project.command
    def execute():
        """ Get execution profile, run & tunneling """

        execution = check_project_execution()
        print("Project Execution detail : ")
        print("  Project name: {}".format(execution['projects_name']))
        print("  Dev Public IP: {}".format(execution['execution_ip']))
        print("  Dev App Session Port: {}".format(execution['execution_port']))
        print("  Domain alias: {}".format(execution['execution_domain']))
        try:
            LOCAL_PORT=81
            print("Tunneling DEV Server App Session to http://localhost:{} ...".format(LOCAL_PORT))
            os.system("python ..\\xen-portforward\port-forward.py {}:{}:{}".format('81',execution['execution_ip'],execution['execution_port']))
        except KeyboardInterrupt:
            sys.exit()
    '''
    @project.command
    def select():
        """ Check select project 

        list_project = check_project_available()
        print("Project Available : ")
        for available in list_project:
            print("  "+available['projects_name']+" -> "+available['projects_git_remoteurl']+" ("+available['projects_git_defaultbranch']+")")
        """
        os.environ["XEN_CURRENT_PROJECT"] '''

    @project.command
    def installed():
        """print all project has installed"""
        data = open("manifest.json","r")
        #print database_table("module_member")
        data_json  = json.loads(data.read())
        if data_json['installed_module'] == []:
            return "no module hasn't installed"
        for available in data_json['installed_module']:
            print(available['name_module'])