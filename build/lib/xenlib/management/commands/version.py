from flask_script import Command, Option
import xenlib

class Version(Command):
    help_args = ('-v', '--version')
    help = "version"

    def __init__(self):
        pass

    def run(self):
        print("xen-tools v0.1")
        pass