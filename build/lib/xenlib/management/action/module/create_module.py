from flask_script import Manager, Command, Option
from xenlib.libs.get_requirement import *
import time
import sys

BASE_DIR = os.path.dirname(os.path.realpath(__name__))
BASE = os.path.join(BASE_DIR, 'xen-project')

class Create(Command):
    help_args = ('-h', '--help')
    help = "Create Module"
    option_list = (
        Option("-r","--remote", dest='remote_git', help='remote git url'),
        Option("-n","--name", required=True, dest='module_name', help='module name'),
        Option("-p","--project", required=True, dest='project_name', help='project name')
    )
    def create_file_init(self,dir=None, file=None, apps=None):
        try:
            with open(os.path.join(dir, file), "a") as f:
                f.write("from xenlib.libs import blueprint\n\n{dir} = blueprint('{dir}', __name__)\n\n".format(dir=apps))
                f.write("@{}.route('/', methods=['POST', 'GET'])".format(apps))
                f.write("\ndef index():\n\treturn \"Hello World\"")
                f.close()
        except Exception as e:
            raise "error creating "

    def run(self, module_name=None, project_name=None, remote_git=None):
        """Create your app module"""
        print("Creating module {module} for project {project}".format(module=module_name, project=project_name))
        project_path = os.path.join(BASE, project_name)
        all_modules_path = os.path.join(project_path, 'modules')
        this_module_path = os.path.join(all_modules_path, module_name)

        if not os.path.exists(all_modules_path):
            os.mkdir(all_modules_path)

        os.mkdir(this_module_path)
        repo = git.Repo(project_path)
        #repo.create_submodule()
        print("... Successfully created module {} ...".format(module_name))
