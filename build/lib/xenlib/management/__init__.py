'''
#!/bin/python

import os
import sys
import unittest
import json
import shutil
import time
import git

from flask_script import Manager

from xenlib import app
from xenlib import config
from xenlib.management.commands.user import Users
from xenlib.management.commands.project import Projects
from xenlib.management.commands.module import Modules
from xenlib import config

manager = Manager(app, usage="Xen Tools Command Line", with_default_commands=False)
manager.help_args = ('-?', '--help')
manager.add_command('workspace', Projects.project)
manager.add_command('user', Users.user)
manager.add_command('project', Projects.project)
manager.add_command('module', Modules.module)

def execute_cli():
    manager.run()'''