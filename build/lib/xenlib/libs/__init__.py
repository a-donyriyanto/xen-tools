import os
from flask import render_template
from flask import redirect
from flask import request
from flask import session
from flask import Blueprint
from flask import Response
from flask import abort

request = request
redirect = redirect
abort = abort
render_template = render_template
session = session
blueprint = Blueprint
response = Response

DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
od_packages = os.path.join(DIR, 'od_packages.json')

def get_root_project():
    try:
        with open('od_packages.json') as data_file:
            data_json = json.loads(data_file.read())
        return data_json['project_root']
    except Exception as e:
        return 'openedoo'
